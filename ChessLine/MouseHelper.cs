﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ChessLine {
    static class MouseHelper {

        //This is a replacement for Cursor.Position in WinForms
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        static extern bool SetCursorPos(int x, int y);

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern void mouse_event(int dwFlags, int dx, int dy, int cButtons, int dwExtraInfo);

        public const int MOUSEEVENTF_LEFTDOWN = 0x02;
        public const int MOUSEEVENTF_LEFTUP = 0x04;

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        [return: System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.Bool)]
        internal static extern bool GetCursorPos(ref Win32Point pt);

        [System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential)]
        internal struct Win32Point {
            public Int32 X;
            public Int32 Y;
        };

        public static Point GetMousePosition() {
            Win32Point w32Mouse = new Win32Point();
            GetCursorPos(ref w32Mouse);
            return new Point(w32Mouse.X, w32Mouse.Y);
        }

        //This simulates a left mouse click
        public static void LeftMouseClick(int xpos, int ypos) {
            SetCursorPos(xpos, ypos);
            mouse_event(MOUSEEVENTF_LEFTDOWN, xpos, ypos, 0, 0);
            mouse_event(MOUSEEVENTF_LEFTUP, xpos, ypos, 0, 0);
        }

        public static void LeftMouseDrag(int xposStart, int yposStart, int xposEnd, int yposEnd, float speed, Action onDone) {
            SetCursorPos(xposStart, yposStart);

            mouse_event(MOUSEEVENTF_LEFTDOWN, xposStart, yposStart, 0, 0);

            float distance = (float)Math.Sqrt(Math.Pow(yposEnd - yposStart, 2) + Math.Pow(xposEnd - xposStart, 2));
            float totalTime = distance / speed;
            float timePassed = 0;

            float interval = .01f;
            while (true) {

                timePassed += interval;

                if (timePassed >= totalTime) { break; }

                float progress = timePassed / totalTime;
                float easedProgress = Ease(progress);
                float newX = xposStart + (xposEnd - xposStart) * easedProgress;
                float newY = yposStart + (yposEnd - yposStart) * easedProgress;
                SetCursorPos((int)newX, (int)newY);

                System.Threading.Thread.Sleep((int)(interval * 1000));
            }

            SetCursorPos(xposEnd, yposEnd);

            mouse_event(MOUSEEVENTF_LEFTUP, xposEnd, yposEnd, 0, 0);

            onDone();
        }

        public static void SetMousePosition(int xpos, int ypos) {
            SetCursorPos(xpos, ypos);
        }

        private static float Ease(float factor) {
            factor /= .5f;
            if (factor < 1) {
                return .5f * factor * factor;
            }
            factor--;
            return -.5f * (factor * (factor - 2) - 1);
        }

    }
}
