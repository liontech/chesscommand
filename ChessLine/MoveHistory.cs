﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ChessLine {

    [Serializable]
    public class Move {
        public string MoveText;
        public int UseCount;
        public Move() { }
        public Move(string moveText) {
            MoveText = moveText;
        }
    }

    [Serializable]
    public class MoveHistory {

        public List<Move> Moves = new List<Move>();

        public MoveHistory() { }

        public void LogMove(string move) {
            GetOrCreateMove(move).UseCount++;
        }

        public string AutoComplete(string input, int index) {
            List<Move> validMoves = Moves.FindAll(x => StringStartsWith(x.MoveText, input));
            if (validMoves.Count == 0) { return string.Empty; }
            validMoves.Sort(delegate (Move a, Move b) { return a.UseCount > b.UseCount ? -1 : 1; });
            Move selectedMove = validMoves[index % validMoves.Count];
            return selectedMove.MoveText.Substring(input.Length);
        }

        private Move GetOrCreateMove(string moveText) {
            Move move = Moves.Find(x => x.MoveText == moveText);
            if (move == null) {
                move = new Move(moveText);
                Moves.Add(move);
            }
            return move;
        }
        
        private static bool StringStartsWith(string value, string prefix) {
            if (prefix.Length > value.Length) { return false; }
            for (int i = 0; i < prefix.Length; i++) {
                if (value[i] != prefix[i]) {
                    return false;
                }
            }
            return true;
        }

    }

}
