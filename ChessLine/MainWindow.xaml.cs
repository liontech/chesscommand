﻿using ChessLine.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ChessLine {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {

        private struct Square {
            public static char[] Files = new char[]{ 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h' };
            public int File;
            public int Rank;

            public bool IsWhite {
                get {
                    return File % 2 != Rank % 2;
                }
            }

            public Square(int file, int rank) {
                File = file;
                Rank = rank;
            }
            public bool IsValid() {
                return File >= 0 && File <= 7 &&
                       Rank >= 0 && Rank <= 7;
            }
        }

        private class Piece {
            public string Name;
            public Square Square;
            public Piece(string name, Square square) {
                Name = name;
                Square = square;
            }
        }

        private List<Piece> pieces = new List<Piece>();
        private bool isPlayingWhite = true;
        private MoveHistory moveHistory;
        private int moveHintIndex = 0;

        public MainWindow() {
            InitializeComponent();
            ResetBoard();
            LoadMoveHistory();
        }

        private void LoadMoveHistory() {
            string moveHistoryXML = Settings.Default["MoveHistory"].ToString();
            if (string.IsNullOrEmpty(moveHistoryXML)) {
                moveHistory = new MoveHistory();
            } else {
                moveHistory = SerializeHelper.Deserialize<MoveHistory>(moveHistoryXML);
            }
        }

        private void SaveMoveHistory() {
            string data = SerializeHelper.Serialize(moveHistory);
            Settings.Default["MoveHistory"] = data;
            Settings.Default.Save();
        }

        private void header_MouseDown(object sender, MouseButtonEventArgs e) {
            try { this.DragMove(); } catch { }
        }

        private void closebutton_Click(object sender, RoutedEventArgs e) {
            this.Close();
        }

        protected override void OnPreviewKeyDown(KeyEventArgs e) {
            if (e.Key == Key.Down) {
                moveHintIndex++;
                UpdateMoveHint();
                return;
            }
            if (e.Key == Key.Up) {
                moveHintIndex--;
                if (moveHintIndex < 0) { moveHintIndex = 0; }
                UpdateMoveHint();
                return;
            }
            if (e.Key == Key.Escape) {
                MoveText.Text = "";
                movetext_TextChanged(null, null);
            }
            base.OnPreviewKeyDown(e);
        }

        private void movetext_KeyDown(object sender, KeyEventArgs e) {
            if (e.Key == Key.Enter) {
                ProcessMoveText();
                return;
            }
            if (e.Key == Key.Space && Keyboard.IsKeyDown(Key.LeftCtrl)) {
                MoveText.Text += AutoCompleteMoveTextHint.Text;   
                movetext_TextChanged(null, null);
                return;
            }
        }

        private void movetext_TextChanged(object sender, TextChangedEventArgs e) {
            CleanBoardCanvas();

            if (MoveText.Text.Contains(" ")) {
                MoveText.Text = MoveText.Text.Replace(" ", "");
                MoveText.CaretIndex = MoveText.Text.Length;
            }

            string moveText = MoveText.Text;
            UpdateMoveHint();
            Piece piece;
            Square targetSquare;
            Piece castleRookPiece;
            Square castleRookSquare;
            if (ProcessMove(moveText, out piece, out targetSquare, out castleRookPiece, out castleRookSquare)) {
                HighlightSquare(piece.Square, Highlight.Piece);
                HighlightSquare(targetSquare, moveText.Contains("x") ? Highlight.Attack : Highlight.Move);
                if (castleRookPiece != null) {
                    HighlightSquare(castleRookPiece.Square, Highlight.Piece);
                    HighlightSquare(castleRookSquare, Highlight.Move);
                }
            } else {
                if (moveText.Length > 0) {
                    List<Piece> piecesToHighlight = new List<Piece>();
                    string moveTextStartCharacter = moveText[0].ToString().ToUpper();
                    if (moveTextStartCharacter == "R") {
                        piecesToHighlight = pieces.FindAll(x => x.Name == "Rook");
                    } else if (moveTextStartCharacter == "N") {
                        piecesToHighlight = pieces.FindAll(x => x.Name == "Knight");
                    } else if (moveText[0] == 'B') { // Bishops have to start with the actual capital letter, else it would interfere with the B pawn
                        piecesToHighlight = pieces.FindAll(x => x.Name == "Bishop");
                    } else if (moveTextStartCharacter == "Q") {
                        piecesToHighlight = pieces.FindAll(x => x.Name == "Queen");
                    } else if (moveTextStartCharacter == "K" || moveTextStartCharacter == "o") {
                        piecesToHighlight = pieces.FindAll(x => x.Name == "King");
                    } else {
                        piecesToHighlight = pieces.FindAll(x => x.Name == "Pawn" &&
                                                                x.Square.File == CharToFile(moveText[0]));
                    }

                    piecesToHighlight.ForEach(x => HighlightSquare(x.Square, Highlight.Piece));
                }
            }

        }

        private void UpdateMoveHint() {
            AutoCompleteMoveTextCopy.Text = MoveText.Text;
            if (string.IsNullOrEmpty(MoveText.Text)) {
                AutoCompleteMoveTextHint.Text = "";
            } else {
                AutoCompleteMoveTextHint.Text = moveHistory.AutoComplete(MoveText.Text, moveHintIndex);
            }
        }

        private void movebutton_Click(object sender, RoutedEventArgs e) {
            ProcessMoveText();
        }

        private void ProcessMoveText() {
            if (MoveText.Text == "white" || MoveText.Text == "black") {
                isPlayingWhite = MoveText.Text == "white";
                ResetBoard();
                TargetInputAndResetMouse();
                moveHistory.LogMove(MoveText.Text);
            }else if (MoveText.Text == "clear") {
                moveHistory = new MoveHistory();
                SaveMoveHistory();
            } else {
                bool validMove = ExecuteMove(MoveText.Text, delegate {
                    TargetInputAndResetMouse();
                });
                if (validMove) {
                    moveHistory.LogMove(MoveText.Text);
                    SaveMoveHistory();
                }
            }
            MoveText.Text = "";
            AutoCompleteMoveTextCopy.Text = "";
            AutoCompleteMoveTextHint.Text = "";
        }

        private void TargetInputAndResetMouse() {
            System.Windows.Point moveTextPosition = MoveText.PointToScreen(new System.Windows.Point(5, 5));
            MouseHelper.LeftMouseClick((int)moveTextPosition.X, (int)moveTextPosition.Y);

            System.Windows.Point moveButtonPosition = MoveButton.PointToScreen(new System.Windows.Point(MoveButton.ActualWidth * .5f,
                                                                                                        MoveButton.ActualHeight * .5f));
            MouseHelper.SetMousePosition((int)moveButtonPosition.X, (int)moveButtonPosition.Y);
        }

        private void ResetBoard() {
            pieces.Clear();
            for (int i = 0; i < 8; i++) {
                pieces.Add(new Piece("Pawn", new Square(i, isPlayingWhite ? 1 : 6)));
            }

            int rank = isPlayingWhite ? 0 : 7;

            pieces.Add(new Piece("Rook", new Square(0, rank)));
            pieces.Add(new Piece("Rook", new Square(7, rank)));

            pieces.Add(new Piece("Knight", new Square(1, rank)));
            pieces.Add(new Piece("Knight", new Square(6, rank)));

            pieces.Add(new Piece("Bishop", new Square(2, rank)));
            pieces.Add(new Piece("Bishop", new Square(5, rank)));

            pieces.Add(new Piece("Queen", new Square(3, rank)));
            pieces.Add(new Piece("King", new Square(4, rank)));
        }

        private bool ProcessMove(string moveString, out Piece piece, out Square targetSquare, out Piece castleRookPiece, out Square targetCastleRookSquare) {

            piece = null;
            targetSquare = new Square(-1, -1);

            castleRookPiece = null;
            targetCastleRookSquare = new Square(-1, -1);

            moveString = moveString.Replace("x", "");

            if (moveString.Length <= 1) {
                LogText.Text = "No move";
                return false;
            }

            // Kingside castle
            if (moveString.ToLower() == "o-o") {
                piece = pieces.Find(x => x.Name == "King");
                targetSquare = new Square(piece.Square.File + 2, piece.Square.Rank);
                castleRookPiece = pieces.Find(x => x.Name == "Rook" && x.Square.File == 7);
                targetCastleRookSquare = new Square(5, castleRookPiece.Square.Rank);
            }

            // Queenside castle
            else if (moveString.ToLower() == "o-o-o") {
                piece = pieces.Find(x => x.Name == "King");
                targetSquare = new Square(piece.Square.File - 2, piece.Square.Rank);
                castleRookPiece = pieces.Find(x => x.Name == "Rook" && x.Square.File == 0);
                targetCastleRookSquare = new Square(3, castleRookPiece.Square.Rank);
            }

            // Bishop
            else if (moveString[0] == 'B') {
                moveString = moveString.Remove(0, 1);
                targetSquare = StringToSquare(moveString);
                Square targetSquareCopy = new Square(targetSquare.File, targetSquare.Rank);
                piece = pieces.Find(x => x.Name == "Bishop" &&
                                         x.Square.IsWhite == targetSquareCopy.IsWhite);
            }

            // Rooks
            else if (char.ToUpper(moveString[0]) == 'R') {
                moveString = moveString.Remove(0, 1);

                List<Piece> rooks = pieces.FindAll(x => x.Name == "Rook");
                if (moveString.Length == 3) {
                    rooks.RemoveAll(x => x.Square.File != CharToFile(moveString[0]) &&
                                         x.Square.Rank != CharToRank(moveString[0]));
                    moveString = moveString.Remove(0, 1);
                }

                targetSquare = StringToSquare(moveString);
                Square targetSquareCopy = new Square(targetSquare.File, targetSquare.Rank);
                piece = rooks.Find(x => x.Square.File == targetSquareCopy.File ||
                                        x.Square.Rank == targetSquareCopy.Rank);
            }

            // Knights
            else if (char.ToUpper(moveString[0]) == 'N') {
                moveString = moveString.Remove(0, 1);

                List<Piece> knights = pieces.FindAll(x => x.Name == "Knight");
                if (moveString.Length == 3) {
                    knights = new List<Piece>() { GetPieceAtFileOrRank("Knight", moveString[0]) };
                    knights.RemoveAll(x => x == null);
                    moveString = moveString.Remove(0, 1);
                }

                targetSquare = StringToSquare(moveString);
                Square targetSquareCopy = new Square(targetSquare.File, targetSquare.Rank);
                piece = knights.Find(x => (x.Square.File == targetSquareCopy.File - 1 && x.Square.Rank == targetSquareCopy.Rank + 2) ||
                                            (x.Square.File == targetSquareCopy.File + 1 && x.Square.Rank == targetSquareCopy.Rank + 2) ||
                                            (x.Square.File == targetSquareCopy.File - 1 && x.Square.Rank == targetSquareCopy.Rank - 2) ||
                                            (x.Square.File == targetSquareCopy.File + 1 && x.Square.Rank == targetSquareCopy.Rank - 2) ||
                                            (x.Square.File == targetSquareCopy.File - 2 && x.Square.Rank == targetSquareCopy.Rank + 1) ||
                                            (x.Square.File == targetSquareCopy.File + 2 && x.Square.Rank == targetSquareCopy.Rank + 1) ||
                                            (x.Square.File == targetSquareCopy.File - 2 && x.Square.Rank == targetSquareCopy.Rank - 1) ||
                                            (x.Square.File == targetSquareCopy.File + 2 && x.Square.Rank == targetSquareCopy.Rank - 1));
            }

            // Queen
            else if (char.ToUpper(moveString[0]) == 'Q') {
                moveString = moveString.Remove(0, 1);

                List<Piece> queens = pieces.FindAll(x => x.Name == "Queen");
                if (moveString.Length == 3) {
                    queens = new List<Piece>() { GetPieceAtFileOrRank("Queen", moveString[0]) };
                    queens.RemoveAll(x => x == null);
                    moveString = moveString.Remove(0, 1);
                }

                targetSquare = StringToSquare(moveString);
                Square targetSquareCopy = new Square(targetSquare.File, targetSquare.Rank);
                piece = queens.Find(x => Math.Abs(x.Square.File - targetSquareCopy.File) == Math.Abs(x.Square.Rank - targetSquareCopy.Rank) ||
                                         x.Square.File == targetSquareCopy.File ||
                                         x.Square.Rank == targetSquareCopy.Rank);
            }

            // King
            else if (char.ToUpper(moveString[0]) == 'K') {
                moveString = moveString.Remove(0, 1);
                piece = pieces.Find(x => x.Name == "King");
                targetSquare = StringToSquare(moveString);
            }

            // Pawn
            else {
                int startFile = CharToFile(moveString[0]);
                int toFile = startFile;
                moveString = moveString.Remove(0, 1);
                if (moveString.Length == 2) {
                    toFile = CharToFile(moveString[0]);
                    moveString = moveString.Remove(0, 1);
                }

                int toRank = CharToRank(moveString[0]);
                List<Piece> pawns = pieces.FindAll(x => x.Name == "Pawn" &&
                                                        x.Square.File == startFile);

                piece = pawns.Find(x => Math.Abs(x.Square.Rank - toRank) == 1);
                if (piece == null) {
                    piece = pawns.Find(x => Math.Abs(x.Square.Rank - toRank) == 2);
                }
                targetSquare = new Square(
                    toFile,
                    toRank
                );
            }

            if (!targetSquare.IsValid()) {
                LogText.Text = "Invalid target square";
                return false;
            }
            if (piece == null) {
                LogText.Text = "Invalid piece";
                return false;
            }

            return true;
        }

        private bool ExecuteMove(string moveString, Action onDone) {
            Piece piece;
            Square targetSquare;
            Piece castleRookPiece;
            Square castleRookSquare;

            if (!ProcessMove(moveString, out piece, out targetSquare, out castleRookPiece, out castleRookSquare)) {
                return false;
            }

            Point fromSquarePosition = GetSquareScreenPosition(piece.Square);
            Point targetSquarePosition = GetSquareScreenPosition(targetSquare);

            MouseHelper.LeftMouseDrag((int)fromSquarePosition.X, (int)fromSquarePosition.Y,
                                      (int)targetSquarePosition.X, (int)targetSquarePosition.Y,
                                      500, delegate {
                piece.Square = targetSquare;

                if (castleRookPiece != null) {
                    castleRookPiece.Square = castleRookSquare;
                }

                // Auto queen promotion
                if (piece.Name == "Pawn" &&
                    (piece.Square.Rank == 7 ||
                     piece.Square.Rank == 0)) {
                    piece.Name = "Queen";
                }

                pieces.RemoveAll(x => x.Square.File == piece.Square.File &&
                                      x.Square.Rank == piece.Square.Rank &&
                                      x != piece);

                onDone();
            });

            return true;
        }

        private Point GetSquareScreenPosition(Square square) {
            float width = (float)BoardRectangle.ActualWidth;
            float height = (float)BoardRectangle.ActualHeight;

            float squareWidth = width / 8;
            float squareHeight = height / 8;

            int file = isPlayingWhite ? square.File : (7 - square.File);
            float offsetX = squareWidth * (file + .5f);

            int rank = isPlayingWhite ? (7 - square.Rank) : square.Rank;
            float offsetY = squareHeight * (rank + .5f);

            System.Windows.Point topLeft = BoardRectangle.PointToScreen(new System.Windows.Point(0, 0));
            return new Point(topLeft.X + offsetX, topLeft.Y + offsetY);
        }

        private Piece GetPieceAtFileOrRank(string pieceName, char fileOrRankChar) {
            List<Piece> potentialPieces = pieces.FindAll(x => x.Name == pieceName);
            potentialPieces.RemoveAll(x => x.Square.File != CharToFile(fileOrRankChar) &&
                                            x.Square.Rank != CharToRank(fileOrRankChar));
            return potentialPieces.Count > 0 ? potentialPieces[0] : null;
        }

        private Square StringToSquare(string squareString) {
            int file = -1;
            for (int i = 0; i < Square.Files.Length; i++) {
                if (squareString.Contains(Square.Files[i].ToString())) { 
                    file = i;
                    break;
                }
            }
            int rank = -1;
            for (int i = 1; i <= 8; i++) {
                if (squareString.Contains(i.ToString())) {
                    rank = i-1;
                    break;
                }
            }
            return new Square(file, rank);
        }

        private int CharToFile(char fileChar) {
            int file = -1;
            for (int i = 0; i < Square.Files.Length; i++){
                if (fileChar == Square.Files[i]) { return i; }
            }
            return file;
        }

        private int CharToRank(char rankChar) {
            int rank;
            bool isRank = int.TryParse(rankChar.ToString(), out rank);
            if (!isRank) { return -1; }
            return rank - 1;
        }

        private enum Highlight {
            Piece,
            Move,
            Attack
        }

        private void HighlightSquare(Square square, Highlight highlight) {
            float width = (float)BoardCanvas.ActualWidth;
            float height = (float)BoardCanvas.ActualHeight;

            float squareWidth = width / 8;
            float squareHeight = height / 8;

            int file = isPlayingWhite ? square.File : (7 - square.File);
            float offsetX = squareWidth * (file);

            int rank = isPlayingWhite ? (7 - square.Rank) : square.Rank;
            float offsetY = squareHeight * (rank);

            Ellipse circle = new Ellipse();
            circle.StrokeThickness = 5;
            switch (highlight) {
                case Highlight.Piece:
                    circle.Stroke = Brushes.Black;
                    break;
                case Highlight.Move:
                    circle.Stroke = Brushes.Yellow;
                    break;
                case Highlight.Attack:
                    circle.Stroke = Brushes.Red;
                    break;
            }
            
            circle.Width = squareWidth;
            circle.Height = squareHeight;

            BoardCanvas.Children.Add(circle);

            Canvas.SetLeft(circle, offsetX);
            Canvas.SetTop(circle, offsetY);
        }

        private void CleanBoardCanvas() {
            BoardCanvas.Children.Clear();
        }

    }
}
